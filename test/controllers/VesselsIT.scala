package controllers

import scala.concurrent._
import duration._
import org.specs2.mutable._

import play.api.libs.json._
import play.api.test._
import play.api.test.Helpers._
import java.util.concurrent.TimeUnit


/**
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class VesselsIT extends Specification {

  val timeout: FiniteDuration = FiniteDuration(5, TimeUnit.SECONDS)

  "Users" should {

    "insert a valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(POST, "/vessel").withJsonBody(Json.obj(
            "name" -> "Prueba_Test",
            "width" -> 1,
            "length" -> 1,
            "draft" -> 1,
            "latitude" -> "1",
            "longitude" -> "1"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status must equalTo(CREATED)
      }
    }

    "fail inserting a non valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(POST, "/vessel").withJsonBody(Json.obj(
          "name" -> "Prueba Test"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "invalid json"
        result.header.status mustEqual BAD_REQUEST
      }
    }

    "update a valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/vessel/Prueba_Test").withJsonBody(Json.obj(
            "name" -> "Prueba_Test",
            "width" -> 2,
            "length" -> 2,
            "draft" -> 2,
            "latitude" -> "2",
            "longitude" -> "2"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status must equalTo(CREATED)
      }
    }

    "fail updating a non valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/vessel/Prueba_Test").withJsonBody(Json.obj(
            "width" -> "3",
            "length" -> "3"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        contentAsString(response.get) mustEqual "invalid json"
        result.header.status mustEqual BAD_REQUEST
      }
    }
    
    "delete a valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/vessel/remove/Prueba_Test").withJsonBody(Json.obj(
            "name" -> "Prueba_Test",
            "width" -> 2,
            "length" -> 2,
            "draft" -> 2,
            "latitude" -> "2",
            "longitude" -> "2"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status must equalTo(CREATED)
      }
    }
    
    "fail deleting a non valid json" in {
      running(FakeApplication()) {
        val request = FakeRequest.apply(PUT, "/vessel/remove/Prueba_Test_2").withJsonBody(Json.obj(
            "name" -> "Prueba_Test",
            "width" -> "2",
            "length" -> "2",
            "draft" -> "2",
            "latitude" -> "2",
            "longitude" -> "2"))
        val response = route(request)
        response.isDefined mustEqual true
        val result = Await.result(response.get, timeout)
        result.header.status mustEqual BAD_REQUEST
      }
    }

  }
}