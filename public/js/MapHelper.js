function addMapInfo(marker, map, element) {
    var contentString = "<p><b>" + element.name + "</b>"
                        + "<p>Width: " + element.width
                        + "<p>Length: " + element.length
                        + "<p>Draft: " + element.draft
                        + "<p> Coordinates: " + element.latitude + ", " + element.longitude
                        + "<p><a href='/#/vessels/map/edit/" + element.name + "' id='btn_vessel_update'><img src='/assets/images/1426862709_698873-icon-136-document-edit-24.png'></img></a>"
                        + "<a href='/#/vessels/map/delete/" + element.name + "' id='btn_vessel_delete'><img src='/assets/images/1426897239_trash.png'></img></a>";
    var infowindow = new google.maps.InfoWindow({
      content: contentString
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });
}