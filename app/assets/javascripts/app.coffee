
dependencies = [
    'ngRoute',
    'ui.bootstrap',
    'myApp.filters',
    'myApp.services',
    'myApp.controllers',
    'myApp.directives',
    'myApp.common',
    'myApp.routeConfig'
]

app = angular.module('myApp', dependencies)

angular.module('myApp.routeConfig', ['ngRoute'])
    .config ($routeProvider) ->
        $routeProvider
            .when('/', {
                templateUrl: '/assets/partials/map.html'
            })
            .when('/list', {
                templateUrl: '/assets/partials/view.html'
            })
            .when('/vessels/list/create', {
                templateUrl: '/assets/partials/create.html'
            })
            .when('/vessels/list/edit/:name', {
                templateUrl: '/assets/partials/update.html'
            })
            .when('/vessels/list/delete/:name', {
                templateUrl: '/assets/partials/delete.html'
            })
            .when('/vessels/map/create', {
                templateUrl: '/assets/partials/create.html'
            })
            .when('/vessels/map/edit/:name', {
                templateUrl: '/assets/partials/update.html'
            })
            .when('/vessels/map/delete/:name', {
                templateUrl: '/assets/partials/delete.html'
            })
            .when('/map', {
                templateUrl: '/assets/partials/map.html'
            })
            .otherwise({redirectTo: '/'})
    .config ($locationProvider) ->
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        })

@commonModule = angular.module('myApp.common', [])
@controllersModule = angular.module('myApp.controllers', [])
@servicesModule = angular.module('myApp.services', [])
@modelsModule = angular.module('myApp.models', [])
@directivesModule = angular.module('myApp.directives', [])
@filtersModule = angular.module('myApp.filters', [])