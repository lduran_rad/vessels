
class MapCtrl

    constructor: (@$log, @VesselService) ->
        @$log.debug "constructing MapController"
        @map = null
        @getAll()

    addmarker: (element) ->
        @marker = new google.maps.Marker({
            position: new google.maps.LatLng(element.latitude, element.longitude),
            animation: google.maps.Animation.DROP,
            map: @map,
            title: element.name
        });
        addMapInfo(@marker, @map,  element);
        
    getAll: () ->
        @$log.debug "getAll()"
        @myPosition = new google.maps.LatLng(0, 0)
        @mapOptions = {
          zoom: 2,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: @myPosition
        };
        @map = new google.maps.Map(document.getElementById("map_canvas"),
            @mapOptions);
        @VesselService.list()
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Vessels"
                @addmarker element for element in data  
            ,
            (error) =>
                @$log.error "Unable to get Vessels: #{error}"
            )
         
        
    

controllersModule.controller('MapCtrl', MapCtrl)
        