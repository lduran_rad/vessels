
class CreateVesselCtrl

    constructor: (@$log, @$location,  @VesselService) ->
        @$log.debug "constructing CreateVesselController"
        @vessel = {}

    create: () ->
        @$log.debug "create()"
        @VesselService.create(@vessel)
        .then(
            (data) =>
                @$log.debug "Promise returned #{data} Vessel"
                @vessel = data
                if @$location.$$path.indexOf("list") != -1
                    @$location.path("/list")
                else 
                    @$location.path("/map")
            ,
            (error) =>
                @$log.error "Unable to create Vessel: #{error}"
            )
    cancel: () ->
        @$log.debug "cancel()"
        if @$location.$$path.indexOf("list") != -1
            @$location.path("/list")
        else 
            @$location.path("/map")

controllersModule.controller('CreateVesselCtrl', CreateVesselCtrl)