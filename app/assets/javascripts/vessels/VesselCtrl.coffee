
class VesselCtrl

    constructor: (@$log, @VesselService) ->
        @$log.debug "constructing VesselController"
        @vessels = []
        @getAll()

    getAll: () ->
        @$log.debug "getAll()"

        @VesselService.list()
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Vessels"
                @vessels = data
            ,
            (error) =>
                @$log.error "Unable to get Vessels: #{error}"
            )

controllersModule.controller('VesselCtrl', VesselCtrl)