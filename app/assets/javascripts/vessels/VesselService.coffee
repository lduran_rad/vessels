
class VesselService

    @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    @defaultConfig = { headers: @headers }

    constructor: (@$log, @$http, @$q) ->
        @$log.debug "constructing VesselService"

    list: () ->
        @$log.debug "list()"
        deferred = @$q.defer()

        @$http.get("/vessels")
        .success((data, status, headers) =>
                @$log.info("Successfully listed Vessels - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to list Vessels - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    create: (vessel) ->
        @$log.debug "create #{angular.toJson(vessel, true)}"
        deferred = @$q.defer()

        @$http.post('/vessel', vessel)
        .success((data, status, headers) =>
                @$log.info("Successfully created vessel - status #{status}")
                deferred.resolve(data)
            )
        .error((data, status, headers) =>
                @$log.error("Failed to create vessel - status #{status}")
                deferred.reject(data)
            )
        deferred.promise

    update: (name, vessel) ->
      @$log.debug "update #{angular.toJson(vessel, true)}"
      deferred = @$q.defer()

      @$http.put("/vessel/#{name}", vessel)
      .success((data, status, headers) =>
              @$log.info("Successfully updated vessel - status #{status}")
              deferred.resolve(data)
            )
      .error((data, status, header) =>
              @$log.error("Failed to update vessel - status #{status}")
              deferred.reject(data)
            )
      deferred.promise
      
    delete: (name, vessel) ->
      @$log.debug "delete #{angular.toJson(vessel, true)}"
      deferred = @$q.defer()

      @$http.put("/vessel/remove/#{name}", vessel)
      .success((data, status, headers) =>
              @$log.info("Successfully deleted vessel - status #{status}")
              deferred.resolve(data)
            )
      .error((data, status, header) =>
              @$log.error("Failed to deleted vessel - status #{status}")
              deferred.reject(data)
            )
      deferred.promise

servicesModule.service('VesselService', VesselService)