class UpdateVesselCtrl

  constructor: (@$log, @$location, @$routeParams, @VesselService) ->
      @$log.debug "constructing UpdateVesselController"
      @vessel = {}
      @find()

  update: () ->
      @$log.debug "updater()"
      @VesselService.update(@$routeParams.name, @vessel)
      .then(
          (data) =>
            @$log.debug "Promise returned #{data} Vessel"
            @vessel = data
            if @$location.$$path.indexOf("list") != -1
                @$location.path("/list")
            else 
                @$location.path("/map")
        ,
        (error) =>
            @$log.error "Unable to update Vessel: #{error}"
      )

  find: () ->
      # route params must be same name as provided in routing url in app.coffee
      name = @$routeParams.name
      @$log.debug "find route params: #{name}"

      @VesselService.list()
      .then(
        (data) =>
          @$log.debug "Promise returned #{data.length} Vessels"

          # find a vessel with the name of firstName and lastName
          # as filter returns an array, get the first object in it, and return it
          @vessel = (data.filter (vessel) -> vessel.name is name)[0]
      ,
        (error) =>
          @$log.error "Unable to get Vessels: #{error}"
      )
      
   delete: () ->
      @$log.debug "delete()"
      @VesselService.delete(@$routeParams.name, @vessel)
      .then(
          (data) =>
            @$log.debug "Promise returned #{data} Vessel"
            @vessel = data
            if @$location.$$path.indexOf("list") != -1
                @$location.path("/list")
            else 
                @$location.path("/map")
        ,
        (error) =>
            @$log.error "Unable to delete Vessel: #{error}"
      )
    cancel: () ->
        @$log.debug "cancel()"
        if @$location.$$path.indexOf("list") != -1
            @$location.path("/list")
        else 
            @$location.path("/map")

controllersModule.controller('UpdateVesselCtrl', UpdateVesselCtrl)