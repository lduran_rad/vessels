package models

case class Vessel(name: String,
                 width: Int,
                 length: Int,
                 draft: Int,
                 latitude: String,
                 longitude: String)

object JsonFormats {
  import play.api.libs.json.Json

  // Generates Writes and Reads for Feed and User thanks to Json Macros
  implicit val vesselFormat = Json.format[Vessel]
}